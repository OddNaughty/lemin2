/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   main.c                                             :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: cwagner <cwagner@student.42.fr>            +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2014/02/13 22:27:50 by cwagner           #+#    #+#             */
/*   Updated: 2014/03/24 12:22:24 by cwagner          ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "lemin.h"
#include <stdio.h>

static int	donormeshit(t_room **room, t_lexer **lexed, char *str, int pipe)
{
	int		(*funcs[5])(t_room **room, t_lexer **lexed, char *str, int flag) =
		{isstart, isend, iscom,	ispipe, istoroom };
	int		i;
	int		ret;

	i = 0;
	while (!pipe && i < 5
		   && (ret = funcs[i](room, lexed, str, pipe)) != SUCCESS)
	{
		if (ret == PIPED)
				pipe = 1;
		if (ret == FAILURE)
			return (FAILURE);
		i++;
	}
	if (i == 5)
		return (error ("Unknown map line"));
	return (pipe);
}

int		tolex(t_room **room, t_lexer **lexed, char *str)
{
	static int	pipe = 0;

	if (ft_strcheck(str, ft_isdigit) == SUCCESS)
		return (error("Ants cannot be in middle of the map :/."));
	if (!pipe)
	{
		if ((pipe = donormeshit(room, lexed, str, pipe)) == FAILURE)
			return (FAILURE);
	}
	else if (pipe)
		return (ispipe(room, lexed, str, pipe));
	return (SUCCESS);
}

void	printmap(int ants, t_lexer **lexed)
{
	t_lexer		*tmp;

	ft_putnbr(ants);
	ft_putchar('\n');
	tmp = *lexed;
	while (tmp)
	{
		ft_putendl(tmp->str);
		tmp = tmp->next;
	}
}

int		get_map(t_lexer **lexed, t_room **room)
{
	char	*str;
	int		ants;

	str = NULL;
	get_next_line(0, &str);
	if (ft_strcheck(str, ft_isdigit) == SUCCESS)
		ants = ft_atoi(str);
	else
		return (error ("I need the ants' number to work"));
	while (get_next_line(0, &str) > 0)
	{
		if (!*str || *str == 'L')
			break ;
		if (tolex(room, lexed, str) == FAILURE)
			break ;
		free(str);
	}
	return (ants);
}

int		main(int ac, char **av)
{
	int			ants;
	t_lexer		*lexed;
	t_room		*room;
	t_room		*end;
	t_path		*adj;

	(void) av;
	if (ac != 1)
		return (error("I'm reading stdin"));
	lexed = NULL;
	room = NULL;
	ants = get_map(&lexed, &room);
	if (!lexed)
		return (error("No rooms"));
	printmap(ants, &lexed);
	end = dodij(&room);
	if (end && end->prev)
		adj = end->prev->torooms;
	else
		return (error("No way out ?"));
	while (adj)
	{
		if (adj->elem == end)
			break ;
		adj = adj->next;
	}
	if (!adj)
		return (error("No path found"));
	sendants(&end, ants);
	return (SUCCESS);
}
