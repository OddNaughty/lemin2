/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   parsing.c                                          :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: cwagner <cwagner@student.42.fr>            +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2014/02/11 15:38:54 by cwagner           #+#    #+#             */
/*   Updated: 2014/03/23 17:15:36 by cwagner          ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "lemin.h"

t_room	*copyroom(t_room *room)
{
	t_room	*new;

	if ((new = (t_room*) malloc(sizeof(t_room))) == NULL)
		return (NULL);
	if ((new->name = ft_strdup(room->name)) == NULL)
		return (NULL);
	new->flag = room->flag;
	new->path = room->path;
	new->x = room->x;
	new->y = room->y;
	new->torooms = NULL;
	new->next = NULL;
	new->prev = NULL;
	return (new);
}

int		addpathtorooms(t_path **room, t_room **add)
{
	t_path	*tmp;

	tmp = *room;
	if (*room == NULL)
		*room = newpath(add);
	else
	{
		while (tmp->next != NULL)
			tmp = tmp->next;
		tmp->next = newpath(add);
		tmp->next->prev = tmp;
	}
	return (SUCCESS);
}

int		addroomtorooms(t_room **copy, t_room *room)
{
	static t_room   *last = NULL;
	t_room          *new;

	if ((new = copyroom(room)) == NULL)
		return (error("Mallocing error"));
	if (!(*copy))
	{
		*copy = new;
		last = *copy;
	}
	else
	{
		new->prev = last;
		last->next = new;
		last = last->next;
	}
	return (SUCCESS);
}

int		isendandstart(t_lexer **lexed)
{
	t_lexer		*tmp;
	int			y;

	tmp = *lexed;
	y = 0;
	while (tmp != NULL)
	{
		if (tmp->token == START || tmp->token == END)
			y++;
		tmp = tmp->next;
	}
	if (y != 2)
		return (FAILURE);
	return (SUCCESS);
}
