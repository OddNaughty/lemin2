/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   sending.c                                          :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: cwagner <cwagner@student.42.fr>            +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2014/03/13 12:24:50 by cwagner           #+#    #+#             */
/*   Updated: 2014/03/23 23:26:55 by cwagner          ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "lemin.h"

static void		display(t_room *room)
{
	ft_putchar('L');
	ft_putnbr(room->ants);
	ft_putchar('-');
	ft_putstr(room->name);
}

static int		if_ants(t_path **path, int running, int ants)
{
	t_path	*tmp;
	int		full;

	tmp = *path;
	full = 0;
	while (tmp)
	{
		if (tmp->next)
				tmp->elem->ants = tmp->next->elem->ants;
		if (!(tmp->next) && (tmp->elem->ants < ants))
			tmp->elem->ants++;
		if (!tmp->next && (running == 0))
			tmp->elem->ants = 1;
		if (!tmp->next && (running >= ants))
			tmp->elem->ants = 0;
		if (tmp->next && tmp->elem->ants)
			display(tmp->elem);
		if (tmp->next && tmp->elem->ants)
			ft_putchar(' ');
		if (tmp->elem->ants)
			full++;
		tmp = tmp->next;
	}
	return (full);
}
static int		runningants(t_path **road, int ants)
{
	t_path	*tmp;
	int		running;
	int		full;

	running = 0;
	full = 1;
	while (full)
	{
		full = 0;
		tmp = *road;
		full = if_ants(&tmp, running, ants);
		running++;
		ft_putchar('\n');
	}
	return (SUCCESS);
}

int		sendants(t_room **end, int ants)
{
	t_room  *tmp;
	int		i;
	t_path	*road;

	tmp = *end;
	road = NULL;
	while (tmp)
	{
		addpathtorooms(&road, &tmp);
		if (tmp->flag == START)
			break ;
		i++;
		tmp = tmp->prev;
	}
	tmp = *end;
	if (runningants(&road, ants) == FAILURE)
		return (FAILURE);
	return (SUCCESS);
}
