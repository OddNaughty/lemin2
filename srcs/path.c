/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   path.c                                             :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: cwagner <cwagner@student.42.fr>            +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2014/02/14 23:19:14 by cwagner           #+#    #+#             */
/*   Updated: 2014/03/23 23:37:26 by cwagner          ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "lemin.h"

t_room		*searchmin(t_room **room)
{
	t_room	*tmp;
	t_room	*start;
	int		min;

	tmp = *room;
	while (tmp && ((tmp->visited == 1) || (tmp->path == -1)))
		tmp = tmp->next;
	if (!tmp)
		return (NULL);
	min = tmp->path;
	while (tmp)
	{
		if ((tmp->visited == 0) && (tmp->path != -1) && (tmp->path <= min))
		{
			start = tmp;
			min = tmp->path;
		}
		tmp = tmp->next;
	}
	return (start);
}

t_path		*newpath(t_room **add)
{
	t_path	*new;

	new = (t_path *) malloc(sizeof(t_path));
	new->elem = *add;
	new->prev = NULL;
	new->next = NULL;
	return (new);
}

t_room		*searchit(t_room **room, int flag)
{
	t_room	*end;
	t_room	*tmp;

	tmp = *room;
	while (tmp)
	{
		if (tmp->flag == flag)
			return (end = tmp);
		tmp = tmp->next;
	}
	return (NULL);
}

t_room		*dodij(t_room **room)
{
	t_room	*start;
	t_path	*pipe;
	t_room	*end;

	end = searchit(room, END);
	start = searchmin(room);
	while (start && (start != end))
	{
		pipe = start->torooms;
		while (pipe != NULL)
		{
			if ((pipe->elem->visited == 0) &&
				((pipe->elem->path > (1 + start->path)) || (pipe->elem->path == -1)))
			{
				pipe->elem->path = start->path + 1;
				pipe->elem->prev = start;
			}
			pipe = pipe->next;
		}
		start->visited = 1;
		start = searchmin(room);
	}
	return (end);
}
