/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   lstlex.c                                           :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: cwagner <cwagner@student.42.fr>            +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2014/02/15 08:06:19 by cwagner           #+#    #+#             */
/*   Updated: 2014/02/15 08:06:27 by cwagner          ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "lemin.h"

static t_lexer	*newelem(char *str, int token)
{
	t_lexer		*new;

	if ((new = (t_lexer *) malloc(sizeof(t_lexer))) == NULL)
		return (NULL);
	if ((new->str = ft_strdup(str)) == NULL)
		return (NULL);
	new->token = token;
	new->next = NULL;
	return (new);
}

int				addtolex(t_lexer **lexer, char *str, int token)
{
	static t_lexer	*last = NULL;

	if (!(*lexer))
	{
		if ((*lexer = newelem(str, token)) == NULL)
			return (error("Mallocing error"));
		last = *lexer;
	}
	else
	{
		if ((last->next = newelem(str, token)) == NULL)
			return (error("Mallocing error"));
		last = last->next;
	}
	return (SUCCESS);
}
