/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   lstparse.c                                         :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: cwagner <cwagner@student.42.fr>            +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2014/02/11 20:20:38 by cwagner           #+#    #+#             */
/*   Updated: 2014/03/23 23:39:50 by cwagner          ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "lemin.h"

int			applypipe(t_room **room, char *str)
{
	char	**split;
	t_room	*tmp;
	t_room	*ptr;

	if ((split = ft_strsplit(str, '-')) == NULL)
		return (FAILURE);
	tmp = ptr = *room;
	while (tmp && (ft_strcmp(tmp->name, split[0]) != SUCCESS))
		tmp = tmp->next;
	while (ptr && (ft_strcmp(ptr->name, split[1]) != SUCCESS))
		ptr = ptr->next;
	if (addpathtorooms(&(tmp->torooms), &ptr) == FAILURE)
		return (FAILURE);
	if (addpathtorooms(&(ptr->torooms), &tmp) == FAILURE)
		return (FAILURE);
	ft_freechartab(&split);
	return (SUCCESS);
}

int			checkroomname(t_room **room, char *str1, char *str2)
{
	t_room	*tmp;
	int		y;

	tmp = *room;
	y = 0;
	while (tmp != NULL)
	{
		if ((ft_strcmp(tmp->name, str1) == SUCCESS)
			|| (ft_strcmp(tmp->name, str2) == SUCCESS))
			y++;
		tmp = tmp->next;
	}
	if (y != 2)
		return (FAILURE);
	return (SUCCESS);
}

int			checkoldroom(t_room **room, t_room *new)
{
	t_room	*tmp;

	tmp = *room;
	while (tmp != NULL)
	{
		if (ft_strcmp(tmp->name, new->name) == SUCCESS)
			return (error("Two rooms got the same name"));
		if ((tmp->x == new->x) && (tmp->y == new->y))
			return (error("Two rooms got the same coordinates"));
		tmp = tmp->next;
	}
	return (SUCCESS);
}

t_room		*newroom(t_room **room, char **split, int i)
{
	t_room	*new;

	if ((new = (t_room*) malloc(sizeof(t_room))) == NULL)
		return (NULL);
	if ((new->name = ft_strdup(split[0])) == NULL)
		return (NULL);
	new->flag = i;
	new->path = (i == START? 0: -1);
	new->ants = 0;
	new->x = ft_atoi(split[1]);
	new->y = ft_atoi(split[2]);
	new->visited = 0;
	new->torooms = NULL;
	new->next = NULL;
	if (!*room)
	new->prev = NULL;
	else
		new->prev = *room;
	return (new);
}

int			addtorooms(t_room **room, char **str, int token)
{
	static t_room	*last = NULL;
	t_room			*new;

	if ((new = newroom(&last, str, token)) == NULL)
		return (error("Mallocing error"));
	if (checkoldroom(room, new) == FAILURE)
		return (FAILURE);
	if (!(*room))
	{
		*room = new;
		last = *room;
	}
	else
	{
		new->prev = last;
		last->next = new;
		last = last->next;
	}
	return (SUCCESS);
}
