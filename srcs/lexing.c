/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   lexing.c                                           :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: cwagner <cwagner@student.42.fr>            +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2014/02/13 21:22:45 by cwagner           #+#    #+#             */
/*   Updated: 2014/03/24 11:30:04 by cwagner          ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "lemin.h"

int		isstart(t_room **room, t_lexer **lexed, char *str, int pipe)
{
	if (pipe)
		return (B_ERROR);
	if (ft_strcmp(str, CMD_START) != SUCCESS)
		return (B_ERROR);
	if (addtolex(lexed, str, START) == FAILURE)
		return (FAILURE);
	if (get_next_line(0, &str) == FAILURE)
		return (error("Can't read the file"));
	if (!*str || *str == 'L')
		return (FAILURE);
	if (istoroom(room, lexed, str, START) != SUCCESS)
		return (FAILURE);
	return (SUCCESS);
}

int		isend(t_room ** room, t_lexer **lexed, char *str, int pipe)
{
	if (pipe)
		return (B_ERROR);
	if (ft_strcmp(str, CMD_END) != SUCCESS)
		return (B_ERROR);
	if (addtolex(lexed, str, END) == FAILURE)
		return (FAILURE);
	if (get_next_line(0, &str) == FAILURE)
		return (FAILURE);
	if (!*str || *str == 'L')
		return (FAILURE);
	if (istoroom(room, lexed, str, END) != SUCCESS)
		return (FAILURE);
	return (SUCCESS);
}

int		istoroom(t_room **room, t_lexer **lexed, char *str, int flag)
{
	char	**split;

	if (flag == 1)
		return (error("Got some problem in the code..."));
	if ((split = ft_strsplitwhite(str)) == NULL)
		return (error("Mallocing error"));
	if (split[2] == NULL)
		return (B_ERROR);
	if ((ft_strcheck(split[1], ft_isdigit) != SUCCESS)
		|| (ft_strcheck(split[2], ft_isdigit) != SUCCESS))
		return (error("Need numbers for coordinates"));
	if (split[3] != NULL)
		return (error("Rooms are in 2D"));
	if (addtorooms(room, split, flag) == FAILURE)
		return (FAILURE);
	ft_freechartab(&split);
	return (addtolex(lexed, str, ROOM));
}

int		ispipe(t_room **room, t_lexer **lexed, char *str, int flag)
{
    char	**split;
	int		ret;

	if (!*lexed)
		return (B_ERROR);
	if ((split = ft_strsplit(str, '-')) == NULL)
		return (error("Mallocing error"));
	if (split[2] != NULL)
		return (B_ERROR);
	if (!*room)
		return (error("I must have rooms before rally them"));
	if (flag == 0)
		flag = PIPE;
	if (split[0] && split[1] && ft_strcmp(split[0], split[1]) != SUCCESS
		&& (split[2] == NULL))
    {
		if (isendandstart(lexed) == FAILURE)
			return (error("No end nor start"));
		ret = checkroomname(room, split[0], split[1]);
		ft_freechartab(&split);
		if ((addtolex(lexed, str, PIPE) == FAILURE) || (ret == FAILURE))
			return (error("One of these rooms don't exist"));
		return (applypipe(room, str));
    }
	return (B_ERROR);
}

int		iscom(t_room **room, t_lexer **lexed, char *str, int flag)
{
	char	*ptr;

	(void) flag;
	(void) room;
	ptr = NULL;
	if (*str != '#')
		return (B_ERROR);
	if (*str == '#' && *(str + 1) == '#')
	{
		ft_putendl_fd("This command will be ignore", 2);
		addtolex(lexed, str, CMD);
		if (get_next_line(0, &ptr) == FAILURE)
			return (error("Reading file error"));
		if (addtolex(lexed, ptr, CMD) == FAILURE)
		{
			free(ptr);
			return (FAILURE);
		}
		else
		{
			free(ptr);
			return (SUCCESS);
		}
	}
	return (addtolex(lexed, str, COM));
}
