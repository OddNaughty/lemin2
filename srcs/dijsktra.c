/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   dijsktra.c                                         :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: cwagner <cwagner@student.42.fr>            +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2014/02/15 03:03:32 by cwagner           #+#    #+#             */
/*   Updated: 2014/02/15 03:21:55 by cwagner          ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "lemin.h"

static t_dij	*newdij(t_room *room)
{
	t_dij	*new;

	if ((new = (t_dij *) malloc(sizeof(t_dij))) == NULL)
		return (NULL);
	if ((new->name = ft_strdup(room->name)) == NULL)
		return (NULL);
	new->path = room->path;
	new->visited = 0;
	new->prev = NULL;
	return (new);
}

int			addtodij(t_dij **tab, t_room *room)
{
	static t_dij	*last = NULL;

	if (!(*tab))
	{
		if ((*tab = newdij(room)) == NULL)
			return (FAILURE);
		last = *tab;
	}
	else
	{
		if ((last->next = newdij(room)) == NULL)
			return (FAILURE);
		last = last->next;
	}
	return (SUCCESS);
}

t_dij		*initdij(t_room **room)
{
	t_room	*tmp;
	t_dij	*tab;

	tmp = *room;
	tab = NULL;
	while (tmp)
	{
		if (addtodij(&tab, tmp) == FAILURE)
			return (NULL);
		tmp = tmp->next;
	}
	return (tab);
}
