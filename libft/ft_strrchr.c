/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_strrchr.c                                       :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: cwagner <marvin@42.fr>                     +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2013/11/21 21:15:28 by cwagner           #+#    #+#             */
/*   Updated: 2013/11/25 17:56:00 by cwagner          ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "libft.h"

char	*ft_strrchr(const char *s, int c)
{
	int				i;
	unsigned char	_c;

	i = ft_strlen(s);
	_c = c;
	if (_c == '\0')
		return ((char *) s + i);
	while (s[i] != _c && i > 0)
	{
		i--;
		if (s[i] == _c)
			return ((char *) s + i);
	}
	return (NULL);
}
