/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_memchr.c                                        :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: cwagner <marvin@42.fr>                     +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2013/11/21 16:56:25 by cwagner           #+#    #+#             */
/*   Updated: 2013/11/26 20:58:35 by cwagner          ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include <string.h>

void	*ft_memchr(const void *s, int c, size_t n)
{
	const unsigned char	*_s;
	unsigned char		_c;
	int					i;

	_s = s;
	_c = c;
	i = 0;
	if (!_s || !n)
		return (NULL);
	while (n--)
	{
		if (_s[i] == _c)
			return ((char *) s + i);
		i++;
	}
	return (NULL);
}
