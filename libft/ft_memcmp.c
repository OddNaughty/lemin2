/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_memcmp.c                                        :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: cwagner <marvin@42.fr>                     +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2013/11/21 18:43:32 by cwagner           #+#    #+#             */
/*   Updated: 2013/11/25 14:50:35 by cwagner          ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include <string.h>

int		ft_memcmp(const void *s1, const void *s2, size_t n)
{
	const unsigned char		*_s1;
	const unsigned char		*_s2;

	_s1 = s1;
	_s2 = s2;
	if (!_s1 || !_s2)
		return (0);
	while ((*_s1 == *_s2) && n-- > 1)
	{
		_s1++;
		_s2++;
	}
	return (*_s1 - *_s2);
}
