/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_memmove.c                                       :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: cwagner <marvin@42.fr>                     +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2013/11/21 16:02:07 by cwagner           #+#    #+#             */
/*   Updated: 2013/12/01 18:59:28 by cwagner          ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "libft.h"

void	*ft_memmove(void *s1, const void *s2, size_t n)
{
	char		*_s1;
	const char	*_s2;

	_s1 = s1;
	_s2 = s2;
	if (ft_strlen(_s1) < ft_strlen(_s2))
	{
		while (n--)
			_s1[n] = _s2[n];
	}
	else
	{
		while (n--)
			*_s1++ = *_s2++;
	}
	return (s1);
}
