/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_memccpy.c                                       :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: cwagner <marvin@42.fr>                     +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2013/11/20 19:59:06 by cwagner           #+#    #+#             */
/*   Updated: 2013/11/25 16:21:01 by cwagner          ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include <string.h>

void	*ft_memccpy(void * s1, const void * s2, int c, size_t n)
{
	const unsigned char		*_s2;
	unsigned char			*_s1;
	unsigned char			_c;
	int						i;

	i = 0;
	_s2 = s2;
	_s1 = s1;
	_c = c;
	if (!_s2 || !_s1)
		return (NULL);
	while (n--)
	{
		if (_s2[i] == _c)
		{
			_s1[i] = _s2[i];
			i++;
			return ((char *) s1 + i);
		}
		_s1[i] = _s2[i];
		i++;
	}
	return (NULL);
}
