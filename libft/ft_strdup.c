/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   strdup.c                                           :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: cwagner <marvin@42.fr>                     +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2013/11/21 20:47:50 by cwagner           #+#    #+#             */
/*   Updated: 2013/12/17 17:49:13 by cwagner          ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "libft.h"

char	*ft_strdup(const char *s1)
{
	char			*_s1;
	unsigned int	len;

	len = ft_strlen(s1);
	_s1 = (char *) malloc ((1 + len) * sizeof(char));
	if (_s1 == NULL)
		return (NULL);
	ft_memcpy(_s1, s1, len + 1);
	return (_s1);
}
